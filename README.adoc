:experimental:

== Introduction

_Disclaimer : Ceci est un projet fictif, la métropole de Toulouse n’a pas mandaté Stack Labs pour un tel projet._


La métropole de Toulouse souhaite disposer d’une application d’analyse de la météo et de la qualité de l’air pour
mesurer et appréhender l’impact des actions écologistes qu’elle met en place (Zones Faible Émission, espaces verts,
bus électriques, ...).

Suite à une première phase de projet, la métropole a mis en place une petite plateforme de données, dans Google Cloud, lui
permettant d'extraire, ingérer et analyser des données de télémétrie issues de plusieurs stations météo présentes
sur le bassin toulousain.

*⚠️ Problème !* Cette application présente plusieurs défauts de conception..

Afin d'y remédier la métropole vous mandate pour auditer l'application existante, *analyser les problèmes présents et
proposer des solutions*. Dans un second temps, leur principal Data Analyst étant en vacances,
la métropole souahite que vous *répondiez à une série de question sur les données déjà ingérées*.

== Architecture

image::docs/archi_diagram.svg[]

Le projet est déployé sur Google Cloud et utilise principalement des composants "cloud natif", serverless et managés par Google. Le déploiement est réalisé avec Terraform dont le code source peut être trouvé dans le dossier `/iac` de ce repo.

L'architecture se compose des services suivants :

* https://cloud.google.com/run[Cloud Run] : contient l'image Docker du connecteur Python qui extrait les données depuis l'API de Toulouse Métropole. L'instanciation de ce service peut être exploré dans le fichier Terraform `iac/run.tf`. Cette image est responsable de collecter pour chaque station météo, toute la donnée depuis la dernière collecte. La donnée est insérée ligne à ligne dans la table BigQuery `tb_telemetry` en utilisant une requête DML (INSERT INTO) et la librairie client BigQuery.

* https://cloud.google.com/scheduler[Cloud Scheduler] : service de Cron qui déclenche le service Cloud Run toutes les 15 minutes.

* https://cloud.google.com/bigquery[BigQuery] : Datawarehouse (base de données analytique). La donnée est organisée en datasets contenants des tables. Pour le projet nous avons un dataset (`ds_toulouse_meteo_data`) et deux tables (`tb_stations` et `tb_telemetry`). La table `tb_stations` contient une description des stations, et la table tb_telemetry contient les données des capteurs de chaque stations.

* https://datastudio.google.com/[Data Studio] : service de visualisation de données. Cet aspect est hors scope de l'exercice.

=== Le connecteur Python

*Objectif et responsabilités* : Collecter de la donnée depuis une API via HTTP. Écrire le résultat dans une table BigQuery.

Le module se base sur le framework FastAPI et dispose de 4 endpoints (voir fichier `/module/src/main.py`)

* POST `/all/export`: Pour chaque station, produit un fichier avec tout l'historique des relevés. Le fichier est écrit dans un bucket (BACKFILL_BUCKET). Cet endpoint est utilisé une fois lors de l'initialisation des tables.
* POST `/all/recent`: Pour chaque station, récupère toutes les télémétries depuis le dernier export et les ajoute à la table BigQuery. La date de dernier export est récupérée dynamiquement avec un MAX(date) de la table destination. L'appel à cet endpoint est schedulé toutes les 15min avec Cloud Scheduler.
* POST `/{station_id}/export`: exporte l'historique des données pour une station_id en particulier
* POST `/{station_id}/recent`: exporte tous les nouveaux enregistrements depuis le dernier export, pour une station donnée.

==== Les principaux fichiers

* `bigquery_helper.py`: Classe facilitant la lecture/écriture depuis Bigquery.
* `connector.py`: classe du connecteur, avec méthode pour effectuer la requête HTTP auprès du service de la métropole
* `constants.py`: variables constantes et variables d'environnement
* `main.py`: point d'entrée, liste des endpoints de l'application
* `service.py`: Lien entre le manager et le connecteur : logique métier d'export de donnée, construction des paramètres de la requête API, filtrage des résultats
* `manager.py`: Lien entre l'endpoint et le service
* `storage_helper.py`: classe facilitant l'écriture de fichier dans un bucket https://cloud.google.com/storage[Cloud Storage].

=== Modèle de donnée

image::docs/data_model_diagram.png[]

Le modèle de données comporte deux tables : une table de "fait" principale, contenant les relevés de télémétrie météo (tb_telemetry) et une table de "dimension", contenant des informations sur les stations. Sur la table de fait, les attributs `id`, `data`, `heure_utc`, `timestamp` ne sont pas fiable ou pertinents. Les autres attributs pourront être utilisés dans des métriques. La dimension de temps est `heure_de_paris` (UTC).

== Problèmes observés

Afin de vous aider, voici une liste non exhaustive des problèmes observés, à vous de traiter tous ou parti de ces problèmes en fonction du temps que vous souhaitez allouer à l'exercice :

* Il semble qu'une partie des données collectées n'est pas correctement ingérée dans la table destination. Plus précisément, il n'y a pas de nouvelles données pour certaines stations et il y a des 'trous' dans les données des stations qui sont collectés.

Google Cloud propose des services de monitoring en qui collecte les logs et rapports d'erreur de la plupart de ces services. Vous trouverez la liste des erreurs collectées au sein du projet dans https://console.cloud.google.com/errors;filter=%5B%5D;time=P1D?project=toulouse-meteo-data-analytics[Error Reporting].

Les logs détaillés du module Cloud Run de collecte de données sont consultables https://console.cloud.google.com/logs/query;query=%2528%0Aresource.type%20%3D%20%22cloud_run_revision%22%0Aresource.labels.service_name%20%3D%20%22toulouse-meteo-connector%22%0Aresource.labels.location%20%3D%20%22europe-west1%22%0Aseverity%3E%3DDEFAULT%0A%2529%0AOR%20%2528%0AprotoPayload.serviceName%3D%22bigquery.googleapis.com%22%0Aseverity%20%3E%3D%20WARNING%0A%2529;timeRange=PT3H;cursorTimestamp=2022-07-13T06:30:11.473430Z?project=toulouse-meteo-data-analytics[ici].

* Les temps de réponses lors des requêtes SQL sont plus élevés qu'attendu. Un certain nombre d'optimisations dans la façon de modéliser les tables peuvent être implémentés pour améliorer les performances de BigQuery.

* Les développeurs se plaignent de la maintenance du code et de la lenteur du processus de collecte.

NOTE: Vous ne pouvez pas effectuer de modification sur l'architecture déployée ! Vous pouvez faire un fork du repo et suggérer les modifications dans le code directement ou rédiger un document à part pour lister votre analyse et vos solutions.

== Data Analytics : un peu de SQL

Le Data Analysts de Toulouse Metropole étant en vacances, le client souhaite que vous répondiez à une série de question en son absence. Il est attendu, pour chaque question d'avoir l'expression SQL.
Si vous avez un doute sur l'interprétation de la question, n'hésitez pas à faire des hypothèses et à les écrire.

1. Combien y a-t-il d’enregistrements en 2021 ?
2. Pourcentage de stations qui ont été mise à jour hier
3. Liste des stations (titre, description) dont la pression max en 2021 a été inférieure à 100000.
4. Liste des station_id de la table télémétries (tb_telemetry) qui ne sont pas référencés dans la table
référentiel des stations (tb_stations)
5. Moyenne de la température par station la semaine dernière calendaire
6. A quelle heure la pression atmosphérique est-elle au minimum en général ?
7. Temps moyen entre 2 mesures par station
8. Donner la moyenne de la température, le premier mois d’exportation pour chaque station
9. Donner la somme cumulée de la pluie sur une fenêtre glissante de 3 jours (précédents), pour chaque jours et chaque station de la semaine n°5 de l’année 2021
10. Donner la liste des plages de jours consécutifs de canicules (température supérieur à 25 degré toute la journée) pour chacune des stations

NOTE: Vos droits vous donne la possibilité d'exécuter des requêtes SQL dans BigQuery (https://console.cloud.google.com/bigquery?project=toulouse-meteo-data-analytics[lien vers la console]), n'hésitez pas à tester sur des données réelles !

== Livrable

* Un document présentant une analyse des problèmes et une série de recommendations. Si c'est possible, les modifications ou annotations pourront être apportés directement dans le code source. Dans ce cas, vous pouvez faire un fork du projet et soumettre l'exercice sous forme de MR.
* La liste des requêtes SQL permettant de répondre aux questions de la métropole.
